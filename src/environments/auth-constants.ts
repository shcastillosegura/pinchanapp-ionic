export class AuthConstants {
  public static readonly AUTH = 'user'
  public static readonly TOKEN = 'token'
}
