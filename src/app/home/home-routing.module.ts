import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { UserDataResolver } from '../resolver/user-data.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    resolve:{
      userData: UserDataResolver
    },
    children: [
      {
        path: 'feed',
        loadChildren: () =>
          import('../pages/principal/principal.module').then(m => m.PrincipalPageModule)
      },

      {
        path: 'settings',
        loadChildren: () =>
          import('../pages/settings/settings.module').then(
            m => m.SettingsPageModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
