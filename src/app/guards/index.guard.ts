import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthConstants} from '../../environments/auth-constants';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IndexGuard implements CanActivate {
  constructor(
    public AuthService: AuthService,
    public router: Router) {}
  canActivate(): Promise<boolean> {
    return new Promise(resolve => {
      this.AuthService
        .get(AuthConstants.AUTH)
        .then(res => {
          if (res) {
            this.router.navigate(['principal']);
            resolve(false);
          } else resolve(true);
        })
        .catch(err => {
          resolve(true);
        });
    });
  }

}

