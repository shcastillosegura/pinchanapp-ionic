import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import {AuthConstants} from '../../environments/auth-constants';
import {AuthService} from '../services/auth.service'

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  constructor(
    public AuthService: AuthService,
    public router: Router)
  {}

  canActivate(): Promise<boolean> {
    return new Promise(resolve => {
      this.AuthService
        .get(AuthConstants.AUTH)
        .then(res => {
          if (res) {
            this.router.navigate(['principal']);

            resolve(true);
          } else {
            this.router.navigate(['home']);
            resolve(false);
          }
        })
        .catch(err => {
          resolve(false);
        });
    });
  }
}
