import { Injectable } from '@angular/core';
import {CanLoad, Route, Router, UrlSegment, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { filter, map, take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(
    private authService: AuthService,
    private router: Router)
  {}

  canLoad(): Observable<boolean> {
    return this.authService.userData$.pipe(
      filter((val) => {
        return val !== null
      }), // Filter out initial Behaviour subject value
      take(1), // Otherwise the Observable doesn't complete!
      map((userData$) => {
        if (userData$) {
          return true;
        } else {
          this.router.navigateByUrl('/home');
          return false;
        }
      })
    );
  }
}
