import { Injectable } from '@angular/core';
import { CanLoad, Router, UrlSegment, UrlTree } from '@angular/router';
import {AuthConstants} from "../../environments/auth-constants";
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})

export class IntroGuard implements CanLoad {

  constructor(
    private router: Router,
    public AuthService: AuthService,
) { }

  async canLoad(): Promise<boolean> {
    const hasSeenIntro = await this.AuthService.get(AuthConstants.TOKEN);
    console.log('existeintroo',hasSeenIntro);
    if (hasSeenIntro) {
      this.router.navigateByUrl('/principal', { replaceUrl:true });
      return false;
    } else {
      return true;
    }
  }
}
