export interface sportGrounds {
  name: string;
  description: string;
  adress: string;
  latitud: string;
  longitud: string;
  photo: string;
  status: string;
}

