import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {IntroGuard} from "./guards/intro.guard";
import {AuthGuard} from "./guards/auth.guard";

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canLoad: [IntroGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'principal',
    loadChildren: () => import('./pages/principal/principal.module').then(m => m.PrincipalPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'registration',
    loadChildren: () => import('./pages/registration/registration.module').then(m => m.RegistrationPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
  },

  {
    path: 'users',
    loadChildren: () => import('./pages/users/users/users.module').then(m => m.UsersPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'index',
    loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'save-users/:id',
    loadChildren: () => import('./pages/users/edit-users/edit-users.module').then(m => m.EditUsersPageModule)
  },
  {
    path: 'save-users',
    loadChildren: () => import('./pages/users/edit-users/edit-users.module').then(m => m.EditUsersPageModule)
  },
  {
    path: 'sport-grounds',
    loadChildren: () => import('./pages/sport-grounds/sport-grounds/sport-grounds.module').then( m => m.SportGroundsPageModule)
  },
  {
    path: 'save-sport-grounds',
    loadChildren: () => import('./pages/sport-grounds/edit-sport-grounds/edit-sport-grounds.module').then( m => m.EditSportGroundsPageModule)
  },
  {
    path: 'save-sport-grounds/:id',
    loadChildren: () => import('./pages/sport-grounds/edit-sport-grounds/edit-sport-grounds.module').then( m => m.EditSportGroundsPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
