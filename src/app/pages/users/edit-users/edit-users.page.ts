import { Component, OnInit } from '@angular/core';
import { ToastService } from '../../../services/toast.service';
import {UserService} from "../../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.page.html',
  styleUrls: ['./edit-users.page.scss'],
})
export class EditUsersPage implements OnInit {
  userForm: FormGroup;
  id: any;
  userData: any;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private toastService: ToastService,
    public fb: FormBuilder,
    private toastController: ToastController,

  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.id)
    this.userForm = this.fb.group({
      id: [''],
      phone: [''],
      document_number: [''],
      name: [''],
      lastname: [''],
      email: [''],
      username: ['']
    });
    if(this.id){
      this.userService.getUserDataById(this.id).subscribe(res => {
        this.userForm.setValue(res);
      });
    }

  }

  ngOnInit() {
    this.getUserData();
  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      icon: 'globe',
      cssClass: 'custom-toast',
    });

    await toast.present();
  }

  getUserData() {
    this.userService.userData().subscribe(
      async (res: any) => {
        console.log('resultado',res);
        await this.userService.changeUserData(res.userData);
        console.log('user data update');
        this.userService.userData$.subscribe((res: any) => {
          console.log('user data respose', res);
          this.userData = res;
        });
      },
      (error: any) => {
        this.toastService.presentToast('Network Issue.');
      }
    );
  }

  async formSubmit() {
    if (!this.userForm.valid) {
      return false;
    } else {
      if(this.id){
        return this.userService.userUpdate(this.userForm.value).subscribe(res => {
          console.log('resultado de update',res)
          this.presentToast('Usuario actualizado');

        //   capturar errores
        },error=>{
          this.presentToast(error.error.message);
        })
      }else{
        return this.userService.createUser(this.userForm.value).subscribe((res: any) => {
            if (res) {
              this.presentToast('Usuario creado');
              this.router.navigate(['users']);
            }
          }
        );
      }
    }
  }

}
