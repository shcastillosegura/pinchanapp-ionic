import {Component, OnInit} from '@angular/core';
import { ToastService } from '../../../services/toast.service';
import {UserService} from "../../../services/user.service";
import {AlertService} from "../../../services/alert.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  userData: any;

  constructor(
    private userService: UserService,
    private toastService: ToastService,
    private alertService: AlertService,

  ) {
    this.getUserData();
  }
  ngOnInit() {
  }

  getUserData() {
    this.userService.userData().subscribe(
      async (res: any) => {
        console.log('resultado',res);
        await this.userService.changeUserData(res.userData);
        console.log('user data update');
        this.userService.userData$.subscribe((res: any) => {
          console.log('user data respose', res);
          this.userData = res;
        });
      },
      (error: any) => {
        this.toastService.presentToast('Network Issue.das');
      }
    );
  }

  userDeleteAction(msgIndex: number, user_id: any) {
    this.alertService
      .presentAlertConfirm('Delete user', 'Do you want to delete this user?')
      .then((res: any) => {
        if (res.role === 'okay') {
          this.userService.userDelete({id:user_id}).subscribe((res: any) => {
            if (res.success) {
              this.userService.deleteUserData(msgIndex);
            }
          });
        }
      });
  }


}
