import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ToastService } from '../../services/toast.service';
import { FeedService } from 'src/app/services/feed.service';
import {MenuController} from "@ionic/angular";
import {Componente} from "../../interfaces/interfaces";


@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})

export class PrincipalPage implements OnInit {
  componentes: Componente[]=[];
  public authUser: any;
  postData = {
    user: '',
    token: ''
  };
  constructor(
    private auth: AuthService,
    private feedService: FeedService,
    private toastService: ToastService,
    private menuCtrl: MenuController,
  ) {}  ngOnInit() {
    this.getFeedData();
  }
  getFeedData() {
    this.feedService.feedData(this.postData).subscribe(
      (res: any) => {
        this.feedService.changeFeedData(res.feedData);
      },
      (error: any) => {
        this.toastService.presentToast('Network Issue.');
      }
    );
  }

  toggleMenu(){
    this.menuCtrl.toggle();
  }
}
























