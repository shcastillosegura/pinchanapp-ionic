import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthConstants} from '../../../environments/auth-constants';
import {AuthService} from '../../services/auth.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  postData = {
    password: '',
    email: '',
  };

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastService: ToastService
  ) {
  }

  ngOnInit() {
  }

  validateInputs() {
    let password = this.postData.password.trim();
    let email = this.postData.email.trim();
    return (
      this.postData.password &&
      this.postData.email &&
      email.length > 0 &&
      password.length > 0
    );
  }

  loginAction() {
    if (this.validateInputs()) {
      this.authService.login(this.postData).subscribe(
        async (res: any) => {
          console.log({res})
          if (res.user) {

            // Storing the User data.
            await this.authService.store(AuthConstants.AUTH, res.user);
            await this.authService.store(AuthConstants.TOKEN, res.token);

            //Esto ayuda para enviar el boolean que necesitan los guards
            this.authService.userData$.next(true);
            this.router.navigate(['principal']);

          } else {
            this.toastService.presentToast('Incorrect username and password.');
          }
        },
        (error: any) => {
          console.log("este es un log de error=======")
          console.log("error:",JSON.stringify(error))
          this.toastService.presentToast('Network Issue.');
        }
      );
    } else {
      this.toastService.presentToast(
        'Please enter username or password.'
      );
    }
  }
}
