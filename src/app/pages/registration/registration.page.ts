import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthConstants} from '../../../environments/auth-constants';
import {AuthService} from '../../services/auth.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  postData = {
    name: '',
    lastname: '',
    email: '',
    password: ''
  };

  constructor(
    private authService: AuthService,
    private toastService: ToastService,
    private router: Router
  ) {}

  ngOnInit() {}

  validateInputs() {
    console.log(this.postData);
    let name = this.postData.name.trim();
    let lastname = this.postData.lastname.trim();
    let password = this.postData.password.trim();
    let email = this.postData.email.trim();
    return (
      this.postData.name &&
      this.postData.lastname &&
      this.postData.password &&
      this.postData.email &&
      name.length > 0 &&
      lastname.length > 0 &&
      email.length > 0 &&
      password.length > 0
    );
  }

  signupAction() {
    if (this.validateInputs()) {
      this.authService.signup(this.postData).subscribe(
        (res: any) => {
          if (res.user) {
// Storing the User data.
            this.authService
              .store(AuthConstants.AUTH, res.user)
              .then(res => {
                this.router.navigate(['principal']);
              });
          } else {
            this.toastService.presentToast(
              'Data alreay exists, please enter new details.'
            );
          }
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    } else {
      this.toastService.presentToast(
        'Please enter email, username or password.'
      );
    }
  }
}
