import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditSportGroundsPage } from './edit-sport-grounds.page';

const routes: Routes = [
  {
    path: '',
    component: EditSportGroundsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditSportGroundsPageRoutingModule {}
