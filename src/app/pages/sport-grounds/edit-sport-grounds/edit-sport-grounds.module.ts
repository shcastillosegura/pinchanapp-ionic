import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditSportGroundsPageRoutingModule } from './edit-sport-grounds-routing.module';

import { EditSportGroundsPage } from './edit-sport-grounds.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EditSportGroundsPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [EditSportGroundsPage]
})
export class EditSportGroundsPageModule {}
