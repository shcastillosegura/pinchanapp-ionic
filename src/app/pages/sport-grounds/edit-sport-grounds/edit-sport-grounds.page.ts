import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {LoadingController, Platform, ToastController} from "@ionic/angular";
import {SportGroundsService} from "../../../services/sport-grounds.service";
import { ToastService } from '../../../services/toast.service';
import {Filesystem, Directory, FileInfo, ReadFileOptions} from '@capacitor/filesystem';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import {AndroidPermissions} from "@awesome-cordova-plugins/android-permissions/ngx";



const IMAGE_DIR = 'stored-images';

interface LocalFile {
  name: string;
  path: string;
  data: string;
}

@Component({
  selector: 'app-edit-sport-grounds',
  templateUrl: './edit-sport-grounds.page.html',
  styleUrls: ['./edit-sport-grounds.page.scss'],
})
export class EditSportGroundsPage implements OnInit {
  SGForm: FormGroup;
  id: any;
  SGData: any;
  images: LocalFile[] = [];

  constructor(
    private androidPermissions: AndroidPermissions,
    private actRoute: ActivatedRoute,
    public fb: FormBuilder,
    private toastController: ToastController,
    private SportGroundsService: SportGroundsService,
    private router: Router,
    private toastService: ToastService,
    private loadingCtrl: LoadingController,
    private plt: Platform,



  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');
    console.log(this.id)
    this.SGForm = this.fb.group({
      id: [''],
      name: [''],
      description: [''],
      adress: [''],
      latitud: [''],
      longitud: [''],
      status: [''],
      photo: [''],
    });
    if(this.id){
      this.SportGroundsService.getSGDataById(this.id).subscribe(res => {
        this.SGForm.setValue(res);
      });
    }
  }

  ngOnInit() {
    this.loadFiles();

  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      icon: 'globe',
      cssClass: 'custom-toast',
    });

    await toast.present();
  }

  async formSubmitSG() {
    if (!this.SGForm.valid) {
      return false;
    } else {
      if(this.id){
        return this.SportGroundsService.SGUpdate(this.SGForm.value).subscribe(res => {
          console.log(res)
          this.presentToast('Sport Grounds actualizado');
        })
      }else{
        return this.SportGroundsService.createSG(this.SGForm.value).subscribe((res: any) => {
            if (res) {
              this.presentToast('Sport Ground creado');
              this.router.navigate(['sport-grounds']);
            }
          }
        );
      }

    }
  }

  // traer las funciones de la càmara o glaerìa

  async loadFiles() {
    this.images = [];

    const loading = await this.loadingCtrl.create({
      message: 'Loading data...'
    });
    await loading.present();

    Filesystem.readdir({
      path: IMAGE_DIR,
      directory: Directory.Data
    })
      .then(
        (result) => {
          this.loadFileData(result.files);
        },
        async (err) => {
          // Folder does not yet exists!
          await Filesystem.mkdir({
            path: IMAGE_DIR,
            directory: Directory.Data
          });
        }
      )
      .then((_) => {
        loading.dismiss();
      });
  }


  // Get the actual base64 data of an image
  // base on the name of the file
  async loadFileData(fileNames: FileInfo[]) {
    for (let f of fileNames) {
      const filePath = `${IMAGE_DIR}/${f.name}`;
      console.log({f,filePath});
      const readFile = await Filesystem.readFile({
        path: filePath,
        // directory: Directory.Data
      });

      this.images.push({
        name: f.name,
        path: filePath,
        data: `data:image/jpeg;base64,${readFile.data}`
      });
    }
  }

  // tomar foto
  async selectImage() {

    await this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );


    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Uri,
      source: CameraSource.Photos // Camera, Photos or Prompt!
    });

    if (image) {
      this.saveImage(image)
    }
  }

  // Create a new file from a capture image
  async saveImage(photo: Photo) {
    const base64Data = await this.readAsBase64(photo);
    if(base64Data){
      const fileName = new Date().getTime() + '.jpeg';
      const savedFile = await Filesystem.writeFile({
        path: `${IMAGE_DIR}/${fileName}`,
        data: base64Data,
        directory: Directory.Data
      });

      // Reload the file list
      // Improve by only loading for the new image and unshifting array!
      this.loadFiles();
    }else{
      console.log('no existe base64, hubo un error')
    }

  }

  // https://ionicframework.com/docs/angular/your-first-app/3-saving-photos
  private async readAsBase64(photo: Photo) {
    if (this.plt.is('hybrid')) {
      const file = await Filesystem.readFile(<ReadFileOptions>{
        path: photo.path
      });

      return file.data;
    }
    else {
      // Fetch the photo, read as a blob, then convert to base64 format
      if(photo.webPath){
        const response = await fetch(photo.webPath);
        const blob = await response.blob();
        return await this.convertBlobToBase64(blob) as string;
      }
      return false;
    }
  }

// Helper function
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });


// Convert the base64 to blob data
// and create  formData with it
  async startUpload(file: LocalFile) {
    const response = await fetch(file.data);
    const blob = await response.blob();
    const formData = new FormData();
    formData.append('image', blob, file.name);
    this.uploadData(formData);
  }

  // Upload the formData to our API
  async uploadData(formData: FormData) {
    const loading = await this.loadingCtrl.create({
      message: 'Uploading image...',
    });
    await loading.present();

    this.SportGroundsService.uploadImage(formData).subscribe(result=>{
      console.log("ya acabo de subir la imagen", result);

    })
  }



  async deleteImage(file: LocalFile) {
    await Filesystem.deleteFile({
      directory: Directory.Data,
      path: file.path
    });
    this.loadFiles();
    this.presentToast('File removed.');
  }
}
