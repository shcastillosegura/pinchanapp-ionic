import { Component, OnInit } from '@angular/core';
import {SportGroundsService} from "../../../services/sport-grounds.service";
import { ToastService } from '../../../services/toast.service';
import {AlertService} from "../../../services/alert.service";
import {ViewDidEnter} from "@ionic/angular";


@Component({
  selector: 'app-sport-grounds',
  templateUrl: './sport-grounds.page.html',
  styleUrls: ['./sport-grounds.page.scss'],
})
export class SportGroundsPage implements OnInit, ViewDidEnter {
  SGData: any;

  constructor(
    private SportGroundsService: SportGroundsService,
    private toastService: ToastService,
    private alertService: AlertService,

  ) {

  }

  ngOnInit() {

  }

  ionViewWillEnter(){

  }
  ionViewDidEnter(): void {
    this.getSGData();
  }

  getSGData() {
    this.SportGroundsService.SGData().subscribe(
      async (res: any) => {
        console.log('resultado',res);
        await this.SportGroundsService.changeSGData(res.sportGroundData);
        console.log('SG data 222222');
        this.SportGroundsService.sgData$.subscribe((res: any) => {
          console.log('SG data respose', res);
          this.SGData = res;
        });
      },
      (error: any) => {
        this.toastService.presentToast('Network Issue.das');
      }
    );
  }




  SGDeleteAction(msgIndex: number, sg_id: any) {
    this.alertService
      .presentAlertConfirm('Eliminar esta SG?', 'Quieres eliminar esta SG?')
      .then((res: any) => {
        console.log(res)
        if (res.role === 'okay') {
          this.SportGroundsService.SGDelete({id:sg_id}).subscribe((res: any) => {
            console.log('resultado delete',res)
            if (res.success) {
              this.SportGroundsService.deleteSGData(msgIndex);
            }
          });
        }
      });
  }


}
