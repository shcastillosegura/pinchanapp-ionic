import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import { users } from "../interfaces/users";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userData$ = new BehaviorSubject<any>([]);

  constructor(private httpService: HttpService) {
  }

  changeUserData(data: any) {
    console.log("changeUserData",data);
    this.userData$.next(data);
  }

  updateUserData(newUser: any) {
    console.log("updateUserData");
    let data = [];
    data.push(newUser);
    let currentUserData = this.getCurrentUserData();
    let newUserUpdateData = data.concat(currentUserData);
    this.changeUserData(newUserUpdateData);
  }

  getCurrentUserData() {
    console.log("getCurrentUserData");
    return this.userData$.getValue();
  }

  deleteUserData(msgIndex: number) {
    let data: string | any[] = [];
    let currentUserData = this.getCurrentUserData();
    currentUserData.splice(msgIndex, 1);
    let newUserUpdateData = data.concat(currentUserData);
    this.changeUserData(newUserUpdateData);
  }

  userData(): Observable<any> {
    console.log('en feeed data 1111');
    return this.httpService.get('users');
  }

  getUserDataById(id: any): Observable<any> {
    console.log('getuserbyid');
    return this.httpService.get('users/'+id);
  }

  userDelete(data: any): Observable<any> {
    return this.httpService.post("users-delete/"+data.id, data);
  }

  userUpdate(data: any): Observable<any> {
    let endPoints = "users/"+data.id;
    return this.httpService.post(endPoints, data);
  }

  createUser(postData: any): Observable<any> {
    return this.httpService.post('users', postData);
  }

}









