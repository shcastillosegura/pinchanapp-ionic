import { TestBed } from '@angular/core/testing';

import { SportGroundsService } from './sport-grounds.service';

describe('SportGroundsService', () => {
  let service: SportGroundsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SportGroundsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
