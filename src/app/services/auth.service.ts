import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, BehaviorSubject} from 'rxjs';
import {HttpService} from './http.service';
import {AuthConstants} from '../../environments/auth-constants';
import { Preferences } from '@capacitor/preferences';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData$ = new BehaviorSubject<boolean>(Boolean(''));
  token= '';

  constructor(
    private httpService: HttpService,
    private router: Router,
  ) {
    this.loadToken();
  }


  // Create and expose methods that users of this service can
  // call, for example:
  public async set(key: string, value: any) {
    await Preferences.set({
      key: key,
      value: value
    });
  }

  // Store the value
  async store(storageKey: string, value: any) {
    console.log({value})
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await this.set(storageKey,encryptedValue);
  }
  // Get the value
  async get(storageKey: string) {
    const ret = await Preferences.get({ key: storageKey });
    console.log('get del storage keyyyy',storageKey)
    console.log('get del storage',ret)
    console.log('get del  ccccc2222222222',ret.value)
    if(ret && ret.value){
      console.log('decode',JSON.parse(unescape(atob(ret.value))))
      return JSON.parse(unescape(atob(ret.value)));
    }else{
      return null;
    }
  }

  async removeStorageItem(storageKey: string) {
    await Preferences.remove({ key: storageKey });
  }

  // Clear storage
  async clear() {
    await Preferences.clear();
  }

  async loadToken() {
    // acà puiede ser xd
    const token = await this.get(AuthConstants.TOKEN);
    console.log('set tokennnn: ', token);
    if (token) {
      console.log('set tokennnn2: ', token);
      this.token = token;
      this.userData$.next(true);
    } else {
      this.userData$.next(false);
    }
  }

  getUserData() {
    this.get(AuthConstants.TOKEN).then(res => {
      this.userData$.next(res);
    });
  }
  login(postData: any): Observable<any> {
    return this.httpService.post('signin', postData);

  }

  signup(postData: any): Observable<any> {
    return this.httpService.post('signup', postData);
  }

  logout() {
    this.removeStorageItem(AuthConstants.AUTH).then(res => {
      this.router.navigate(['/signin']);
    });
  }

}
