import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpService} from "./http.service";
import { sportGrounds } from "../interfaces/sport-grounds";

@Injectable({
  providedIn: 'root'
})
export class SportGroundsService {

  sgData$ = new BehaviorSubject<any>([]);

  constructor(
    private httpService: HttpService
  ) {
  }

  changeSGData(data: any) {
    console.log("changeSGData",data);
    this.sgData$.next(data);
  }

  getCurrentSGData() {
    console.log("getCurrentSGData");
    return this.sgData$.getValue();
  }

  updateSGData(newSG: any) {
    console.log("updateSGData");
    let data = [];
    data.push(newSG);
    let currentSGData = this.getCurrentSGData();
    let newSGUpdateData = data.concat(currentSGData);
    this.changeSGData(newSGUpdateData);
  }

  deleteSGData(msgIndex: number) {
    let data: string | any[] = [];
    let currentSGData = this.getCurrentSGData();
    currentSGData.splice(msgIndex, 1);
    let newSGUpdateData = data.concat(currentSGData);
    this.changeSGData(newSGUpdateData);
  }

  SGData(): Observable<any> {
    console.log('en sgdata data 1111');
    return this.httpService.get('sport_grounds');
  }

  getSGDataById(id: any): Observable<any> {
    console.log('getsgbyid');
    return this.httpService.get('sport_grounds/'+id);
  }

  SGDelete(data: any): Observable<any> {
    return this.httpService.post("delete/sport_grounds/"+data.id, data);
  }

  SGUpdate(data: any): Observable<any> {
    let endPoints = "sport_grounds/"+data.id;
    return this.httpService.post(endPoints, data);
  }

  createSG(SGData: any): Observable<any> {
    return this.httpService.post('create/sport_grounds/', SGData);
  }

  uploadImage(formData: FormData): Observable<any> {
    return this.httpService.postFormData('upload', formData);
  }

}
