import {Component, Input, OnInit} from '@angular/core';
import { AlertService } from '../../services/alert.service';
import { FeedService } from '../../services/feed.service';

@Component({
  selector: 'app-feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.scss'],
})
export class FeedCardComponent  implements OnInit {
  @Input() loginUser: any;
  feedData: any;
  postData = {
    user: '',
    token: '',
    feed_id: ''
  };
  constructor(
    private feedService: FeedService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    console.log('feed data');
    this.feedService.feedData$.subscribe((res: any) => {
      console.log('feed data respose', res);
      this.feedData = res;
    });
  }

  feedDeleteAction(msgIndex: number, feed_id: any) {
    this.alertService
      .presentAlertConfirm('Delete feed', 'Do you want to delete this feed?')
      .then((res: any) => {
        if (res.role === 'okay') {
          this.feedService.feedDelete({id:feed_id}).subscribe((res: any) => {
            if (res.success) {
              this.feedService.deleteFeedData(msgIndex);
            }
          });
        }
      });
  }

}


