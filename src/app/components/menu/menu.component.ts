import {Component, OnInit} from '@angular/core';
import {AuthConstants} from "../../../environments/auth-constants";
import {Router} from "@angular/router";
import {AlertService} from "../../services/alert.service";
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  componentes: any = [];

  constructor(
    private router: Router,
    private alertService: AlertService,
    private AuthService : AuthService ,

  ) {

  }

  async ngOnInit() {
    this.componentes = [{
        "icon": "beaker-outline",
        "name": "Users",
        "redirectTo": "/users"
      },
      {
        "icon": "card-outline",
        "name": "Settings",
        "redirectTo": "/settings"
      },
      {
        "icon": "football-outline",
        "name": "Sport Grounds",
        "redirectTo": "/sport-grounds"
      },
      {
        "icon": "chatbubbles-outline",
        "name": "Inbox",
        "redirectTo": "/sport-grounds"
      },
    ]
    ;
  }

  logoutClicked( ) {
    this.alertService
      .presentAlertConfirm('Cerrar sesiòn', 'Querès cerrar la sesiòn?')
      .then(async (res: any) => {
        if (res.role === 'okay') {
          await this.AuthService.removeStorageItem(AuthConstants.AUTH);
          await this.AuthService.removeStorageItem(AuthConstants.TOKEN);
          console.log('cerrar sesion', res)
          this.router.navigate(['home']);
        }
      });
    // console.log("Logout");
    // this.storageService.removeStorageItem(AuthConstants.AUTH );
    // this.storageService.removeStorageItem(AuthConstants.TOKEN);
    // this.router.navigate(['login']);
  }
}



















