import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-startbutton',
  templateUrl: './startbutton.component.html',
  styleUrls: ['./startbutton.component.scss'],
})
export class StartbuttonComponent  implements OnInit {

  constructor(private router: Router) {}

  ngOnInit() {}
  navigateToLogin() {
    this.router.navigate(['/login']);
  }
}
