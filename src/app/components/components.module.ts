import { CommonModule } from '@angular/common';
import {NgModule, Pipe} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TimeAgoPipe } from 'time-ago-pipe';

import { FeedCardComponent } from './feed-card/feed-card.component';
import { FeedUpdateComponent } from './feed-update/feed-update.component';

import { SlidesComponent } from './slides/slides.component';
import { StartbuttonComponent } from './startbutton/startbutton.component';
import { TimeagoComponent } from './timeago/timeago.component';
import {MenuComponent} from "./menu/menu.component";
import {RouterModule} from "@angular/router";

@Pipe({
  name: 'timeAgo',
  pure: false
})
export class TimeAgoExtendsPipe extends TimeAgoPipe {}
@NgModule({
  declarations: [
    SlidesComponent,
    StartbuttonComponent,
    FeedCardComponent,
    FeedUpdateComponent,
    TimeagoComponent,
    TimeAgoExtendsPipe,
    MenuComponent
  ],
  exports: [
    SlidesComponent,
    StartbuttonComponent,
    FeedCardComponent,
    FeedUpdateComponent,
    TimeagoComponent,
    MenuComponent,
    RouterModule
  ],
  imports: [CommonModule, FormsModule, IonicModule]
})
export class ComponentsModule {}











