import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import {Storage} from "@ionic/storage";
import {ComponentsModule} from "./components/components.module";
import {AndroidPermissions} from "@awesome-cordova-plugins/android-permissions/ngx";
// import {FeedCardComponent} from "./components/feed-card/feed-card.component";

@NgModule({
  declarations: [AppComponent],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, ComponentsModule, ReactiveFormsModule, FormsModule],
  providers: [HttpClientModule, StatusBar,SplashScreen, Storage,AndroidPermissions, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
